package control.grasp.Vendas.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import control.grasp.Vendas.models.Stock;
import control.grasp.Vendas.services.StockService;
import lombok.AllArgsConstructor;
import lombok.Data;

@RestController
@RequestMapping("/stock")
public class StockController {
    private StockService stockService;

    public StockController(StockService productService) {
        this.stockService = productService;
    }

    @GetMapping()
    public ResponseEntity<List<Stock>> getAll() {
        return ResponseEntity.ok().body(stockService.getAll());
    }

    @PostMapping("/buy")
    public ResponseEntity<Stock> buy(@RequestBody ProductToStock form) {
        Stock stock = stockService.buy(form.getProductId(), form.getQuantity());
        if (stock != null) {
            return ResponseEntity.ok().body(stock);
        } else {
            System.out.println("Supplier doesn't have this product!");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/sell")
    public ResponseEntity<Stock> sell(@RequestBody ProductToStock form) {
        return ResponseEntity.ok().body(stockService.sell(form.getProductId(), form.getQuantity()));
    }
}

@Data
class ProductToStock {
    private Long productId;
    private int quantity;
}
