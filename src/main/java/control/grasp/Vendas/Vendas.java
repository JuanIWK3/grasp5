package control.grasp.Vendas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vendas {

	public static void main(String[] args) {
		SpringApplication.run(Vendas.class, args);
	}
}
