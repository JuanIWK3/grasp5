package control.grasp.Vendas.services;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import control.grasp.Vendas.models.Product;
import control.grasp.Vendas.repositories.ProductRepo;

@Service
public class ProductService {
  private final ProductRepo productRepo;

  @Autowired
  public ProductService(ProductRepo productRepo) {
    this.productRepo = productRepo;
  }

  @Transactional
  public List<Product> getAll() {
    return productRepo.findAll();
  }

  @Transactional
  public Product addProduct(Product product) {
    return productRepo.save(product);
  }

}
