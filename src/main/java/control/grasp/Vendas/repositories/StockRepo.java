package control.grasp.Vendas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import control.grasp.Vendas.models.Stock;

public interface StockRepo extends JpaRepository<Stock, Long> {

}