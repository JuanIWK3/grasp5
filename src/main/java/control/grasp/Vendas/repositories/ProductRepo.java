package control.grasp.Vendas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import control.grasp.Vendas.models.Product;

public interface ProductRepo extends JpaRepository<Product, Long> {

}