package control.grasp.Vendas.services;

import java.util.List;
import java.util.Optional;

import org.hibernate.annotations.NotFound;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import control.grasp.Vendas.models.Product;
import control.grasp.Vendas.models.Stock;
import control.grasp.Vendas.repositories.ProductRepo;
import control.grasp.Vendas.repositories.StockRepo;

@Service
public class StockService {
  private final StockRepo stockRepo;
  private final ProductRepo productRepo;

  @Autowired
  public StockService(StockRepo stockRepo, ProductRepo productRepo) {
    this.stockRepo = stockRepo;
    this.productRepo = productRepo;
  }

  @Transactional
  public List<Stock> getAll() {
    return stockRepo.findAll();
  }

  @Transactional
  public Stock buy(Long productId, int quantity) {
    Optional<Product> product = productRepo.findById(productId);
    Optional<Stock> stock = stockRepo.findById(productId);

    if (product.isPresent()) {
      Product p = product.get();
      if (stock.isPresent()) {
        Stock existentStock = stock.get();
        existentStock.setQuantity(existentStock.getQuantity() + quantity);
        return stockRepo.save(existentStock);
      } else {
        return stockRepo.save(new Stock(p.getId(), p.getName(), p.getPrice(), quantity));
      }
    } else {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Supplier doesn't have this product!");
    }

  }

  @Transactional
  public Stock sell(Long productId, int quantity) {
    Optional<Stock> stock = stockRepo.findById(productId);

    if (stock.isPresent()) {
      Stock existentStock = stock.get();
      if (existentStock.getQuantity() == quantity) {
        stockRepo.deleteById(productId);
        throw new ResponseStatusException(HttpStatus.OK, "All stock ");
      } else if (existentStock.getQuantity() < quantity) {
        throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Insufficient quantity!");
      } else {
        System.out.println("Sold!");
        existentStock.setQuantity(existentStock.getQuantity() - quantity);
        return stockRepo.save(existentStock);
      }
    } else {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Product not in stock!");
    }
  }
}