package control.grasp.Vendas.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stock implements Serializable {
  @Id
  private Long id;
  private String name;
  private double price;
  private int quantity;
}